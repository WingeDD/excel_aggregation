import sys
import copy
import json
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import (
    QApplication, QMainWindow, QPushButton, QMessageBox, QLabel, QLineEdit, QVBoxLayout, QFileDialog, QWidget, QPlainTextEdit
)
from cli import get_json_data, __default_config_file
from aggregator import aggregate


class MainWindow(QMainWindow):
    BOOK_PATH_KEY: str = "book_path"
    SOURCE_SHEET_KEY: str = "source_sheet"


    def __init__(self, config: dict):
        super().__init__()

        self.__config = copy.deepcopy(config)
        if self.BOOK_PATH_KEY in self.__config:
            del self.__config[self.BOOK_PATH_KEY]
        if self.SOURCE_SHEET_KEY in self.__config:
            del self.__config[self.SOURCE_SHEET_KEY]
        self.book_fpath = None

        self.setWindowTitle("table aggregation")
        global_widget = self.create_global_widget()
        # self.setMinimumSize(QSize(600, 400))
        self.setCentralWidget(global_widget)


    def create_global_widget(self) -> QWidget:
        global_widget = QWidget(self)
        global_layout = QVBoxLayout()
        global_widget.setLayout(global_layout)


        # book file selection
        book_label = QLabel(global_widget)
        book_label.setText("Файл книги:")
        book_label.setWordWrap(True)
        global_layout.addWidget(book_label)

        file_name = QLabel(global_widget)
        file_name.setText("<файл не выбран>")
        # file_name.setWordWrap(True)
        file_name.setTextInteractionFlags(Qt.TextInteractionFlag.TextSelectableByMouse)
        global_layout.addWidget(file_name)

        file_btn = QPushButton("выбрать файл", global_widget)
        def set_book_file_callback():
            try:
                fpath = QFileDialog.getOpenFileName(self)
                if fpath == ('', ''):
                    QMessageBox.warning(self, "Файл книги", "Файл не был выбран", QMessageBox.StandardButton.Ok,  QMessageBox.StandardButton.Ok)
                else:
                    self.book_fpath = fpath[0]
                    file_name.setText(fpath[0])
            except Exception as e:
                QMessageBox.warning(self, "Файл книги", repr(e), QMessageBox.StandardButton.Ok,  QMessageBox.StandardButton.Ok)
        file_btn.clicked.connect(set_book_file_callback)
        # file_btn.setMaximumWidth(200)
        global_layout.addWidget(file_btn)


        # sheet name input
        sheet_label = QLabel(global_widget)
        sheet_label.setText("Название листа:")
        sheet_label.setWordWrap(True)
        global_layout.addWidget(sheet_label)
        sheet_input = QLineEdit(global_widget)
        global_layout.addWidget(sheet_input)


        # other attributes text
        other_label = QLabel(global_widget)
        other_label.setText("Другое:")
        other_label.setWordWrap(True)
        global_layout.addWidget(other_label)
        other_attrs_text = QPlainTextEdit(global_widget)
        other_attrs_text.setPlainText(json.dumps(self.__config, indent=4))
        global_layout.addWidget(other_attrs_text)


        # submit button
        submit_btn = QPushButton("Запуск", global_widget)
        def on_submit():
            self.setEnabled(False)
            try:
                cfg = json.loads(other_attrs_text.toPlainText())
                cfg[self.BOOK_PATH_KEY] = self.book_fpath
                cfg[self.SOURCE_SHEET_KEY] = sheet_input.text()
                aggregate(cfg)
                QMessageBox.information(self, "Готово", f"Файл {cfg[self.BOOK_PATH_KEY]} перезаписан", QMessageBox.StandardButton.Ok,  QMessageBox.StandardButton.Ok)
            except Exception as e:
                QMessageBox.warning(self, "Ошибка", repr(e), QMessageBox.StandardButton.Ok,  QMessageBox.StandardButton.Ok)
            finally:
                self.setEnabled(True)
        submit_btn.clicked.connect(on_submit)
        # submit_btn.setMaximumWidth(150)
        submit_btn.setMinimumHeight(50)
        global_layout.addWidget(submit_btn)

        return global_widget



def run_gui(q_rgs: list, config_fpath: str = __default_config_file):
    cfg = get_json_data(config_fpath)
    app = QApplication(q_rgs)
    window = MainWindow(cfg)
    window.show()
    app.exec()


def launch(config_fpath: str = __default_config_file) -> None:
    run_gui(sys.argv, config_fpath)
