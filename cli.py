import sys
import os
import json
from aggregator import aggregate


__script_dir: str = os.path.dirname(os.path.abspath(__file__))
__default_config_file: str = os.path.join(__script_dir, "config", "config.json")


def get_config_file(default: str = None) -> str:
    if len(sys.argv) > 2:
        raise Exception(f'expected 0 or 1 command line argument, got: {len(sys.argv)-1}.')
    elif len(sys.argv) == 2:
        return sys.argv[1]
    else:
        return default


def get_json_data(fname: str, encoding='utf-8-sig') -> dict:
    with open(fname, "r", encoding=encoding) as file:
        return json.load(file)


def launch() -> None:
    config: dict = get_json_data(get_config_file(__default_config_file))
    aggregate(config)
