from typing import Hashable
import openpyxl


def excel_col_letter_to_num(letter: str) -> int:
    return ord(letter.upper()) - 64 # ord('A') == 65


def write_aggregated_data(
        data: dict[Hashable, dict[Hashable, dict[int, object]]],
        book_path: str,
        titles: list[tuple[int, object]],
        aggregation_sheet_prefix: str,
        id_col_aggregation: int
    ) -> None:
    wb: openpyxl.Workbook = openpyxl.load_workbook(book_path)
    for sheet_name in data:
        full_sheet_name: str = f"{aggregation_sheet_prefix}_{sheet_name}"
        ws: openpyxl.worksheet.worksheet.Worksheet = wb.create_sheet(title=full_sheet_name)
        for t in titles:
            ws.cell(row=1, column=t[0]).value = t[1]

        cur_row: int = 2
        sheet_data: dict[Hashable, dict[int, object]] = data[sheet_name]
        for id in sheet_data:
            ws.cell(row=cur_row, column=id_col_aggregation).value = id
            col_data: dict[int, object] = sheet_data[id]
            for col in col_data:
                ws.cell(row=cur_row, column=col).value = col_data[col]
            cur_row += 1

    wb.save(book_path)


def aggregate(config: dict):
    book_path: str = config["book_path"]
    source_sheet: str = config["source_sheet"]
    aggregation_sheet_prefix: str = config["aggregation_sheet_prefix"]

    id_col_source: int = excel_col_letter_to_num(config["id_col_source"])
    id_col_aggregation: int = excel_col_letter_to_num(config["id_col_aggregation"])
    sheet_separation_col: str = excel_col_letter_to_num(config["sheet_separation_col"])
    copy_columns_proxy: dict[str, str] = config["copy_columns"]
    copy_columns: list[tuple[int, int]] = [] # [(src_col, aggregation_col), (src_col, aggregation_col), ...]
    for k in copy_columns_proxy:
        copy_columns.append((excel_col_letter_to_num(k), excel_col_letter_to_num(copy_columns_proxy[k])))
    sum_columns_proxy: dict[str, str] = config["sum_columns"]
    sum_columns: list[tuple[int, int]] = [] # [(src_col, aggregation_col), (src_col, aggregation_col), ...]
    for k in sum_columns_proxy:
        sum_columns.append((excel_col_letter_to_num(k), excel_col_letter_to_num(sum_columns_proxy[k])))

    empty_lines_check_limit: int = config["empty_lines_check_limit"]


    wb: openpyxl.Workbook = openpyxl.load_workbook(book_path, read_only=True)
    src_sheet: openpyxl.worksheet._read_only.ReadOnlyWorksheet = wb[source_sheet]
    src_rows = src_sheet.rows

    src_title_row: list = [cell.value for cell in next(src_rows)]
    titles: list[tuple[int, object]] = [] # [(agg_col, val), (agg_col, val), ...]
    titles.append( ( id_col_aggregation, src_title_row[id_col_source-1] ) )
    for src_agg_col in copy_columns:
        titles.append( (src_agg_col[1], src_title_row[src_agg_col[0]-1]) )
    for src_agg_col in sum_columns:
        titles.append( (src_agg_col[1], src_title_row[src_agg_col[0]-1]) )


    aggregated: dict[Hashable, dict[Hashable, dict[int, object]]] = {} # sheet_separation_col -> id -> agg_col -> val
    def check_init_nested_dict(id_sheet, id) -> bool:
        if id_sheet not in aggregated:
            aggregated[id_sheet] = {}
        if id not in aggregated[id_sheet]:
            aggregated[id_sheet][id] = {}
            return False
        return True
    empty_lines: int = 0
    for src_row in src_rows:
        curr_src_row: list = [cell.value for cell in src_row]
        id = curr_src_row[id_col_source-1]
        if id is None:
            empty_lines += 1
            if empty_lines > empty_lines_check_limit:
                break
            else:
                continue
        id_sheet = curr_src_row[sheet_separation_col-1]
        already_inited: bool = check_init_nested_dict(id_sheet, id)
        curr_agg_res: dict[int, object] = aggregated[id_sheet][id]
        if already_inited:
            for src_agg_col in sum_columns:
                curr_agg_res[src_agg_col[1]] += float(curr_src_row[src_agg_col[0]-1])
        else:
            for src_agg_col in copy_columns:
                curr_agg_res[src_agg_col[1]] = curr_src_row[src_agg_col[0]-1]
            for src_agg_col in sum_columns:
                curr_agg_res[src_agg_col[1]] = float(curr_src_row[src_agg_col[0]-1])

        empty_lines = 0

    wb.close()
    write_aggregated_data(aggregated, book_path, titles, aggregation_sheet_prefix, id_col_aggregation)
